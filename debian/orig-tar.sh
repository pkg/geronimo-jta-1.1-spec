#!/bin/sh -e

# $2 = version
TAR=../geronimo-jta-1.1-spec_$2.orig.tar.gz
DIR=libgeronimo-jta-1.1-spec-java-$2.orig
TAG=geronimo-jta_1.1_spec-$2

# clean up the upstream tarball
svn export http://svn.apache.org/repos/asf/geronimo/specs/tags/$TAG/ $DIR
GZIP=--best tar -c -z -f $TAR $DIR
rm -rf $DIR
rm ../$TAG

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir
  echo "moved $TAR to $origDir"
fi
